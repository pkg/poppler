Source: poppler
Section: devel
Priority: optional
Maintainer: Debian freedesktop.org maintainers <pkg-freedesktop-maintainers@lists.alioth.debian.org>
Uploaders: Loic Minier <lool@dooz.org>,
           Pino Toscano <pino@debian.org>,
           Emilio Pozuelo Monfort <pochu@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-gir,
               dpkg-dev (>= 1.22.5),
               cmake,
               gir1.2-cairo-1.0-dev,
               gir1.2-gio-2.0-dev,
               gir1.2-gobject-2.0-dev,
               gtk-doc-tools (>= 1.14),
               libboost-dev (>= 1.71.0),
               libcairo2-dev (>= 1.16.0),
               libcurl4-gnutls-dev,
               libfontconfig-dev,
               libfreetype-dev,
               libgdk-pixbuf-2.0-dev,
               libgirepository1.0-dev (>= 1.64.0),
               libglib2.0-dev (>= 2.64),
               libgpgme-dev,
               libgpgmepp-dev (>= 1.19.0),
               libgtk-3-dev,
               libjpeg-dev,
               liblcms2-dev,
               libnss3-dev,
               libpng-dev,
               libtiff-dev,
               pkgconf,
               python3:any,
               qt6-base-dev [amd64 arm64 armel armhf loong64 mips64el ppc64el riscv64 s390x alpha hppa hurd-any m68k powerpc ppc64 sh4 sparc64],
               qtbase5-dev,
               zlib1g-dev
Build-Depends-Indep: libcairo2-doc, libglib2.0-doc
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://poppler.freedesktop.org/
Vcs-Git: https://salsa.debian.org/freedesktop-team/poppler.git
Vcs-Browser: https://salsa.debian.org/freedesktop-team/poppler

Package: libpoppler145
Architecture: any
Section: libs
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: poppler-data
Suggests: gpgsm (>= 2.2.42)
Description: PDF rendering library
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains the shared core library.

Package: libpoppler-dev
Architecture: any
Section: libdevel
Multi-Arch: same
Depends: libpoppler145 (= ${binary:Version}),
         ${misc:Depends}
Description: PDF rendering library -- development files
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains the development libraries needed to build applications
 using Poppler.

Package: libpoppler-private-dev
Architecture: any
Section: libdevel
Multi-Arch: same
Depends: libpoppler-dev (= ${binary:Version}), ${misc:Depends}
Suggests: libfreetype-dev
Description: PDF rendering library -- private development files
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains the private and unstable headers needed to build
 applications using the private Poppler core library.

Package: libpoppler-glib8t64
Provides: ${t64:Provides}
Breaks: libpoppler-glib8 (<< 24.02.0-3~)
Replaces: libpoppler-glib8 (<< 24.02.0-3~)
Architecture: any
Section: libs
Multi-Arch: same
Depends: libpoppler145 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: PDF rendering library (GLib-based shared library)
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package provides the GLib-based shared library for applications
 using the GLib interface to Poppler.

Package: libpoppler-glib-dev
Architecture: any
Section: libdevel
Depends: libcairo2-dev (>= 1.16.0),
         libglib2.0-dev (>= 2.64),
         libpoppler-dev (= ${binary:Version}),
         libpoppler-glib8t64 (= ${binary:Version}),
         ${gir:Depends},
         ${misc:Depends}
Provides: ${gir:Provides}
Suggests: libpoppler-glib-doc
Description: PDF rendering library -- development files (GLib interface)
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains the headers and development libraries needed to
 build applications using the GLib-based Poppler interface.

Package: libpoppler-glib-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Recommends: libcairo2-doc, libglib2.0-doc
Multi-Arch: foreign
Description: PDF rendering library -- documentation for the GLib interface
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains the API documentation of the GLib-based Poppler
 interface.

Package: gir1.2-poppler-0.18
Architecture: any
Section: introspection
Multi-Arch: same
Depends: ${gir:Depends}, ${misc:Depends}
Description: GObject introspection data for poppler-glib
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains introspection data for poppler-glib.
 .
 It can be used by packages using the GIRepository format to generate
 dynamic bindings.

Package: libpoppler-qt5-1t64
Provides: ${t64:Provides}
Breaks: libpoppler-qt5-1 (<< 24.02.0-3~)
Replaces: libpoppler-qt5-1 (<< 24.02.0-3~)
Architecture: any
Section: libs
Multi-Arch: same
Depends: libpoppler145 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: PDF rendering library (Qt 5 based shared library)
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package provides the Qt 5 based shared library for applications
 using the Qt 5 interface to Poppler.

Package: libpoppler-qt5-dev
Architecture: any
Section: libdevel
Multi-Arch: same
Depends: libpoppler-dev (= ${binary:Version}),
         libpoppler-qt5-1t64 (= ${binary:Version}),
         qtbase5-dev,
         ${misc:Depends}
Description: PDF rendering library -- development files (Qt 5 interface)
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains the headers and development libraries needed to
 build applications using the Qt 5-based Poppler interface.

Package: libpoppler-qt6-3t64
Provides: ${t64:Provides}
Breaks: libpoppler-qt6-3 (<< 24.02.0-3~)
Replaces: libpoppler-qt6-3 (<< 24.02.0-3~)
Architecture: hurd-any alpha amd64 arm64 armel armhf hppa loong64 m68k mips64el powerpc ppc64 ppc64el riscv64 s390x sh4 sparc64
Section: libs
Multi-Arch: same
Depends: libpoppler145 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: PDF rendering library (Qt 6 based shared library)
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package provides the Qt 6 based shared library for applications
 using the Qt 6 interface to Poppler.

Package: libpoppler-qt6-dev
Architecture: hurd-any alpha amd64 arm64 armel armhf hppa loong64 m68k mips64el powerpc ppc64 ppc64el riscv64 s390x sh4 sparc64
Section: libdevel
Multi-Arch: same
Depends: libpoppler-dev (= ${binary:Version}),
         libpoppler-qt6-3t64 (= ${binary:Version}),
         qt6-base-dev,
         ${misc:Depends}
Description: PDF rendering library -- development files (Qt 6 interface)
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains the headers and development libraries needed to
 build applications using the Qt 6-based Poppler interface.

Package: libpoppler-cpp2
Architecture: any
Section: libs
Multi-Arch: same
Depends: libpoppler145 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: PDF rendering library (CPP shared library)
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package provides the CPP shared library for applications
 using a simple C++ interface (using STL, and no other dependency) to Poppler.

Package: libpoppler-cpp-dev
Architecture: any
Section: libdevel
Multi-Arch: same
Depends: libpoppler-cpp2 (= ${binary:Version}),
         libpoppler-dev (= ${binary:Version}),
         ${misc:Depends}
Description: PDF rendering library -- development files (CPP interface)
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains the headers and development libraries needed to
 build applications using the C++ Poppler interface.

Package: poppler-utils
Architecture: any
Section: utils
Multi-Arch: foreign
Depends: libpoppler145 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: xpdf-utils (<< 3.02-2~)
Conflicts: pdftohtml
Replaces: pdftohtml, xpdf-reader, xpdf-utils (<< 3.02-2~)
Provides: pdftohtml, xpdf-utils
Description: PDF utilities (based on Poppler)
 Poppler is a PDF rendering library based on Xpdf PDF viewer.
 .
 This package contains command line utilities (based on Poppler) for getting
 information of PDF documents, convert them to other formats, or manipulate
 them:
  * pdfdetach -- lists or extracts embedded files (attachments)
  * pdffonts -- font analyzer
  * pdfimages -- image extractor
  * pdfinfo -- document information
  * pdfseparate -- page extraction tool
  * pdfsig -- verifies digital signatures
  * pdftocairo -- PDF to PNG/JPEG/PDF/PS/EPS/SVG converter using Cairo
  * pdftohtml -- PDF to HTML converter
  * pdftoppm -- PDF to PPM/PNG/JPEG image converter
  * pdftops -- PDF to PostScript (PS) converter
  * pdftotext -- text extraction
  * pdfunite -- document merging tool
